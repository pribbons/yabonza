export class Celebrity{
    rank:number;
    name:string;
    netWorth:number;
    age:string;
    country:string;
}