import { Component } from '@angular/core';
import { RichListService } from './rich-list.service';
import {IPageHeaderDetails} from './rich-list.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'app';
  
  PageHeaderDetails : IPageHeaderDetails

  constructor(private richListService: RichListService) { }

  ngOnInit(){
    this.PageHeaderDetails=this.richListService.getPageHeaderDetails();
  }

}
