import {Celebrity} from './Celebrity'

export class CelebrityRiches {
  pageTitleH1:string;
  pageTitleH2:string;
  description:string;
  referenceLink:string;
  usDollarValue:string;
  australianDollarValue:string;
  euroValue:string;
  celebrityList:Celebrity[] ;
}


