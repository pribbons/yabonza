import { Component, OnInit } from '@angular/core';

import { RichListService } from '../rich-list.service';
import { Celebrity} from '../celebrity';
import {ExchangeRate} from '../rich-list.service';

@Component({
  selector: 'app-filtered-list',
  templateUrl: './filtered-list.component.html',
  styleUrls: ['./filtered-list.component.scss']
})
export class FilteredListComponent implements OnInit {

  //view data
  Celebrities : Celebrity[];
  Countries : string[];
  ExchangeRates : ExchangeRate;
  
  //filtering options
  SelectedCountry ="";
  SearchString = "";
  SelectedCurrency= "USD";
  SelectedOrder = "rank";

  DisplayData: Celebrity[];

  

  constructor(private richListService: RichListService) { }

  ngOnInit(){
    //load data from repository
    this.Celebrities=this.richListService.getCelebrities();
    this.Countries=this.richListService.getCountries();
    this.ExchangeRates=this.richListService.getCurrencyExchange();
    this.FilterSortData();
  }

  FilterSortData(){
      //first sort
      this.DisplayData=this.Celebrities.slice().sort((a,b)=>{ 
          return a[this.SelectedOrder]<b[this.SelectedOrder] ? -1:1;
        });

       if (!!this.SelectedCountry){
         this.DisplayData=this.DisplayData.filter(c=>c.country==this.SelectedCountry);
       }

       if (!!this.SearchString)
       {
         this.DisplayData=this.DisplayData.filter(c=>{
            return c.age.includes(this.SearchString)||  c.country.includes(this.SearchString)|| c.name.includes(this.SearchString)||(`{c.netWorth}`).includes(this.SearchString);
         })
       }


  }

}
