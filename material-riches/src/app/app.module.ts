import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatCardModule, MatListModule, MatSelectModule, MatInputModule,MatIconModule} from '@angular/material';


import { AppComponent } from './app.component';
import { FilteredListComponent } from './filtered-list/filtered-list.component';
import { RichListService } from './rich-list.service';

@NgModule({
  declarations: [
    AppComponent,
    FilteredListComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    MatCardModule, MatListModule, MatSelectModule, MatInputModule,MatIconModule
  ],
  providers: [RichListService],
  bootstrap: [AppComponent]
})
export class AppModule { }
