import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CelebrityRiches } from './celebrity-riches';
import { CELEBRITY_RICHES } from './mock-app-richest';
import {Celebrity} from './celebrity';

@Injectable()
export class RichListService {

  //Todo: once only xhr request on initialise and return observables
  CelebrityData : CelebrityRiches = CELEBRITY_RICHES;
  
  constructor() { }

  getPageHeaderDetails():IPageHeaderDetails{
     return this.CelebrityData;
  }

  getCurrencyExchange():ExchangeRate{
      var e=new ExchangeRate;
      e.USD=this.CelebrityData.usDollarValue;
      e.AUD=this.CelebrityData.australianDollarValue;
      e.EURO=this.CelebrityData.euroValue;
      return e;
  }

  getCountries():string[]{
    //return unique list of countries
    return this.CelebrityData.celebrityList.map(item => item.country).filter((value, index, self) => self.indexOf(value) === index).sort();
  }

  getCelebrities():Celebrity[]{
      return this.CelebrityData.celebrityList;
  }

}

export class IPageHeaderDetails{
  pageTitleH1:string;
  pageTitleH2:string;
  description:string;
  referenceLink:string;
}

export class ExchangeRate{
  USD:string;
  AUD:string;
  EURO:string;
}
